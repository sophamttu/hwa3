import java.util.Iterator;
import java.util.LinkedList;
import java.util.stream.DoubleStream;

public class DoubleStack {
   private LinkedList<Double> list;

   public static void main (String[] argum) {
      DoubleStack ds = new DoubleStack();
      System.out.print(ds.interpret("5. 4. 3. /"));
   }

   DoubleStack() {
      list = new LinkedList<>();
   }

   @Override
   public DoubleStack clone() throws CloneNotSupportedException{
/*      DoubleStack tmp = new DoubleStack();
      tmp.list.addAll(list);*/
      DoubleStack tmp = new DoubleStack();
      tmp.list = (LinkedList<Double>)list.clone();
      return tmp;
   }

   public boolean stEmpty() {
      return list.isEmpty();
   }

   public void push (double a) {
      list.addFirst(a);
   }

   public double pop() {
      if (list.isEmpty())
         throw new IndexOutOfBoundsException("stack underflow");
      double v = list.getFirst();
      list.removeFirst();
      return v;
   } // pop

   public void op (String s) {
      if(list.size() < 1) throw new IndexOutOfBoundsException("too few elements for " + s);
      Double op2 = pop();
      Double op1 = pop();
      switch (s) {
         case "+":
            push(op1 + op2);
            break;
         case "-":
            push(op1 - op2);
            break;
         case "*":
            push(op1 * op2);
            break;
         case "/":
            if(op2 == 0)
               throw new RuntimeException("Division by 0: " + s);
            push(op1 / op2);
            break;
         default:
            throw new IllegalArgumentException("Invalid operation: " + s);
      }
   }

   public double tos() {
      if (list.isEmpty())
         throw new RuntimeException("stack underflow");
      return list.getFirst();
   }

   @Override
   public boolean equals (Object o) {
      if(o == null || !(o.getClass().equals(DoubleStack.class))) return false;
      //LinkedList<Double> tmp = ((DoubleStack) o).list;
      return list.equals(((DoubleStack) o).list);
   }

   @Override
   public String toString() {
      if(list.isEmpty()) return "Empty";
      StringBuilder s = new StringBuilder();
      for(int i = list.size() - 1; i >= 0; --i) {
         s.append(list.get(i)).append(" ");
      }
      if(s.length() != 0)
         s.deleteCharAt(s.length()-1);
      return s.toString();
   }

   public static double interpret (String pol) {
      DoubleStack ds = new DoubleStack();
      pol = pol.trim();
      if (pol.isEmpty()) {
         throw new RuntimeException("Expression missing: " + pol);
      }
      String[] ss = pol.split("\\s+");
      for (String s : ss) {
         if (s.equals("+") || s.equals("*") || s.equals("-") || s.equals("/"))
            ds.op(s);
         else
            try {
               double d = Double.parseDouble(s);
               ds.push(d);
            } catch (Exception e) {
               throw new RuntimeException("illegal symbol " + s + " in "+  pol);
            }
      }
      if (ss.length < 3) {
         try {
            throw new RuntimeException("too few elements: " + pol);
         } catch(Exception e){
            return ds.tos();
         }
      }
      if (ds.list.size() != 1)
         throw new IndexOutOfBoundsException("leave redundant elements: " + pol);
      return ds.tos();
   }
}